{-# LANGUAGE UnicodeSyntax #-}
module GitLab.Network (manager) where
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import System.IO.Unsafe


-- | Network manager, used internally by all API calls.
--
-- I considered including it into 'GitLab' type, but some API
-- calls can be performed unathenticated.
--
-- So, for now, this ugly solution.
manager ∷ Manager
manager = unsafePerformIO newTlsManager
{-# NOINLINE manager #-}
