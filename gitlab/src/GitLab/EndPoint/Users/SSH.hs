{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UnicodeSyntax     #-}
module GitLab.EndPoint.Users.SSH (list, add_) where
import           Control.Monad
import           Control.Monad.Catch
import           Data.Aeson
import           Data.Text.Encoding        (encodeUtf8)
import qualified GitLab.Network            as Net
import           GitLab.Types
import           Network.HTTP.Client
import           Network.HTTP.Types
import           Network.HTTP.Types.Status

list ∷ GitLab → IO [ASSHKey]
list (GitLab token) = do
  let request = defaultRequest {
        method = "GET",
        secure = True,
        host = "gitlab.com",
        port = 443,
        path = "api/v4/user/keys",
        requestHeaders = [("Private-Token", encodeUtf8 token)]
      }
  withResponse request Net.manager $ \response -> do
    let status = responseStatus response
    body <- responseBody response
    when (status /= ok200) $ do
      throwM $ GitLabException request Nothing status body
    case decodeStrict body of
      Nothing   -> throwM $ GitLabException request Nothing status body
      Just keys -> pure keys

add_ ∷ GitLab → Q_SSH_Key_Add → IO ASSHKey
add_ (GitLab token) query = do
 let request = defaultRequest {
        method = "POST",
        secure = True,
        host = "gitlab.com",
        port = 443,
        path = "api/v4/user/keys",
        queryString = renderQuery False (toQuery query),
        requestHeaders = [("Private-Token", encodeUtf8 token)]
      }
 withResponse request Net.manager $ \response -> do
   let status = responseStatus response
   body <- responseBody response
   when (status /= created201) $ do
     throwM $ GitLabException request Nothing status body
   case decodeStrict body of
     Nothing   -> throwM $ GitLabException request Nothing status body
     Just keys -> pure keys
