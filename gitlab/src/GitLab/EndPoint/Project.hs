{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UnicodeSyntax     #-}
module GitLab.EndPoint.Project (create_) where
import           Control.Monad
import           Control.Monad.Catch
import           Data.Aeson
import           Data.Text.Encoding        (encodeUtf8)
import           GitLab.JSON.Encode
import qualified GitLab.Network            as Net
import           GitLab.Types
import           Network.HTTP.Client
import           Network.HTTP.Types
import           Network.HTTP.Types.Status

filterQuery ∷ Query → Query
filterQuery = filter (\(_, val) -> val /= Nothing)

create_ ∷ GitLab → QProjectCreate → IO ()
create_ (GitLab token) query = do
  let request = defaultRequest {
        method = "POST",
        secure = True,
        host   = "gitlab.com",
        port   = 443,
        path   = "api/v4/projects",
        requestHeaders = [("Private-Token", encodeUtf8 token)],
        queryString = renderQuery False (filterQuery.toQuery $ query)
      }
  withResponse request Net.manager $ \response -> do
    let status = responseStatus response
    when (status /= ok200) $ do
      body <- responseBody response
      throwM $ GitLabException request Nothing status body
    responseBody response >>= print
