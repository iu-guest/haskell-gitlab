{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE UnicodeSyntax         #-}
module GitLab.Types where
import Control.Exception
import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Data.Bool
import Data.ByteString              (ByteString)
import Data.Char
import Data.Empty
import Data.String                  (fromString)
import Data.Text                    (Text)
import Data.Text.Encoding           (encodeUtf8)
import Data.TH.Query
import Data.These
import GHC.Generics
import Network.HTTP.Client
import Network.HTTP.Types.QueryLike
import Network.HTTP.Types.Status

newtype Name = Name Text
newtype Path = Path Text
newtype GitLab = GitLab Text -- ^ Access token

-- | This exception is raised by API calls, when server returns
-- anything, but status code 200. As much, as possible context is
-- provided to facilate debugging.
data GitLabException = GitLabException {
  _request  :: Request,
  _json     :: Maybe Value,
  _status   :: Status,
  _response :: ByteString
} deriving Show
instance Exception GitLabException

instance QueryValueLike Bool where
  toQueryValue = Just . fromString . bool "false" "true"

data Visibility = Public | Internal | Private deriving Show
instance QueryValueLike Visibility where
  toQueryValue = Just . fromString . map toLower . show
instance QueryValueLike Int where
  toQueryValue = Just . fromString . show

data ProjectEdit = ProjectEdit {
  _defaultBranch                             :: Maybe Text,
  _description                               :: Maybe Text,
  _issuesEnabled                             :: Maybe Bool,
  _mergeRequestsEnabled                      :: Maybe Bool,
  _jobsEnabled                               :: Maybe Bool,
  _wikiEnabled                               :: Maybe Bool,
  _snippetsEnabled                           :: Maybe Bool,
  _resolveOutdatedDiffDiscussions            :: Maybe Bool,
  _containerRegistryEnabled                  :: Maybe Bool,
  _sharedRunnersEnabled                      :: Maybe Bool,
  _visibility                                :: Maybe Visibility,
  _importUrl                                 :: Maybe Text,
  _publicJobs                                :: Maybe Bool,
  _onlyAllowMergeIfPipelineSucceeds          :: Maybe Bool,
  _onlyAllowMergeIfAllDiscussionsAreResolved :: Maybe Bool,
  _mergeMethod                               :: Maybe Text,
  _lfsEnabled                                :: Maybe Bool,
  _requestAccessEnabled                      :: Maybe Bool,
  -- XXX: How to make it play nicely to $(deriveQuery)
  -- _tagList                                :: Maybe [Text],
  _printingMergeRequestLinkEnabled           :: Maybe Bool,
  _ciConfigPath                              :: Maybe Text,
  _approvalsBeforeMerge                      :: Maybe Int
}

data QProjectCreate = QProjectCreate {
  _namepath    :: These Name Path,
  _namespaceID :: Maybe Int,
  _edit        :: ProjectEdit
}

data ASSHKey = ASSHKey {
  id        :: Int,
  title     :: Text,
  key       :: Text,
  createdAt :: Text
}

data Q_SSH_Key_Add = Q_SSH_Key_Add {
  _title :: Text,
  _key   :: Text
}

deriveToJSON (defaultOptions { constructorTagModifier = map toLower }) ''Visibility
deriveToJSON (defaultOptions { omitNothingFields = True,
                               fieldLabelModifier = tail.camelTo2 '_'}) ''ProjectEdit
deriveFromJSON (defaultOptions { fieldLabelModifier = tail.camelTo2 '_'}) ''ASSHKey

makeLenses ''ProjectEdit
deriveQuery ''ProjectEdit
deriveQuery ''Q_SSH_Key_Add

(.->) ∷ String → Text → (ByteString, Maybe ByteString)
p .-> v = (fromString p, Just (encodeUtf8 v))

instance QueryLike (These Name Path) where
  toQuery = \case
    This (Name name) -> ["name" .-> name]
    That (Path path) -> ["path" .-> path]
    These (Name name) (Path path) -> ["name" .-> name, "path" .-> path]

instance QueryLike QProjectCreate where
  toQuery (QProjectCreate np nsId edit_) = concat [
      toQuery np,
      toQuery [("namespace_id", nsId)],
      toQuery edit_
    ]

-- It is important to have these definitions /below/, due yet-another
-- TemplateHaskell limitation (https://ghc.haskell.org/trac/ghc/ticket/9813)

emptyProjectEdit ∷ ProjectEdit
emptyProjectEdit = $(mkNothing ''ProjectEdit)
