{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UnicodeSyntax     #-}
module GitLab.JSON.Encode where
import           Data.Aeson
import qualified Data.HashMap.Strict as HM
import           Data.These
import           GitLab.Types

-- Here I deliberately avoid ToJSON class. Aeson is the only option when
-- it comes to JSON, but I believe that law-less classes complicate code
-- for no return.

jsonQProjectCreate ∷ QProjectCreate → Value
jsonQProjectCreate = \case
  QProjectCreate namepath_ namepathID_ edit_ ->
    let Object obj1 = case namepath_ of
                 This (Name name) -> object ["name" .= name]
                 That (Path path) -> object ["path" .= path]
                 These (Name name) (Path path) -> object ["name" .= name, "path" .= path]
        Object obj2 = case namepathID_ of
                 Just id_ -> object ["namespace_id" .= id_]
                 Nothing  -> object []
    -- XXX: non-exhaustive pattern match
        Object obj3 = toJSON edit_ in
    Object $ obj1 `HM.union` obj2 `HM.union` obj3
