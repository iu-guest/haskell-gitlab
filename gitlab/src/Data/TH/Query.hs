{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UnicodeSyntax   #-}
module Data.TH.Query (deriveQuery) where
import Data.Aeson
import Language.Haskell.TH
import Language.Haskell.TH.Lib
import Network.HTTP.Types

nameToParameter ∷ Name → String
nameToParameter = camelTo2 '_'.tail.nameBase

mkToQuery ∷ [Name] → Q Exp
mkToQuery fields = do
  let pat = varP (mkName "arg")
      arg = varE (mkName "arg")
      convert exp = [| toQuery [ $exp ] |]
      process1 field = convert [| ($(stringE (nameToParameter field)), $(varE field) $arg) |]
      body = varE 'concat `appE` listE (map process1 fields)
  lam1E pat body

deriveQuery ∷ Name → Q [Dec]
deriveQuery name = do
  TyConI (DataD _ _ _ _ [RecC _ vars] _) <- reify name
  let fields = map (\(n, _, _) -> n) vars
  [d| instance QueryLike $(conT name) where toQuery = $(mkToQuery fields) |]
