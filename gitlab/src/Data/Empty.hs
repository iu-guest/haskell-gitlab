{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UnicodeSyntax   #-}
module Data.Empty (mkNothing) where
import Language.Haskell.TH
import Language.Haskell.TH.Lib


mkNothingValue ∷ Name → Int → Q Exp
mkNothingValue cname count =
  let nothing     = conE 'Nothing
      values      = take count $ repeat nothing
      constructor = conE cname in
  appsE $ constructor:values

-- | Create value of single-constructor datatype with every field
-- in form 'Maybe a'.
mkNothing ∷ Name → Q Exp
mkNothing name = do
  TyConI (DataD _ _ _ _ [con] _) <- reify name
  let (cname, count) = case con of
                         NormalC cname fields -> (cname, length fields)
                         RecC    cname fields -> (cname, length fields)
  mkNothingValue cname count
