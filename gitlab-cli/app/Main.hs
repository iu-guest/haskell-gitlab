{-# LANGUAGE LambdaCase         #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UnicodeSyntax      #-}
module Main where
import Application.Handlers (process)
import Application.Options  (parser)
import Options.Applicative

main ∷ IO ()
main = execParser parser >>= process
