{-# LANGUAGE LambdaCase    #-}
{-# LANGUAGE UnicodeSyntax #-}
module Application.Handlers (process) where
import qualified Application.Handlers.Project as Project
import qualified Application.Handlers.SSH     as SSH
import           Application.Options.Types

process ∷ Action → IO ()
process = \case
  ActionProject a -> Project.process a
  ActionSSH a     -> SSH.process a
