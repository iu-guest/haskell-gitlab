{-# LANGUAGE UnicodeSyntax #-}
module Application.Env (gitlab) where
import           Control.Monad
import           Data.Text     (Text)
import qualified Env
import           GitLab.Types

token ∷ Env.Parser Env.Error Text
token = Env.var (Env.str <=< Env.nonempty) "GITLAB_TOKEN" (Env.help msg) where
  msg = unlines [
      "Secret access token. Due security consideration, it must be passed ",
      "as environment variable, not via command line."
    ]

gitlab ∷ IO GitLab
gitlab = Env.parse (Env.header "Command line interface to GitLab instances") $
  GitLab <$> token
