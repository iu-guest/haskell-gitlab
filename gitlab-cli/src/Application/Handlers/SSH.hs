{-# LANGUAGE LambdaCase      #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UnicodeSyntax   #-}
module Application.Handlers.SSH (process) where
import qualified Application.Env           as Env
import           Application.Options.Types
import           Control.Monad
import qualified Data.Text.IO              as T
import qualified GitLab.EndPoint.Users.SSH as EndPoint
import           GitLab.Types

printKey ∷ ASSHKey → IO ()
printKey (ASSHKey {..}) = print key

process ∷ ActionSSH → IO ()
process = \case
  ActionSSHList -> do
    g <- Env.gitlab
    keys <- EndPoint.list g
    mapM_ printKey keys
  ActionSSHAdd {..} -> do
    g <- Env.gitlab
    key' <- case key of
              Left key'' -> pure key''
              Right path -> T.readFile path
    EndPoint.add_ g (Q_SSH_Key_Add title key') >>= printKey
  _ -> error "Not implemented"
