{-# LANGUAGE LambdaCase    #-}
{-# LANGUAGE UnicodeSyntax #-}
module Application.Handlers.Project (process) where
import qualified Application.Env           as Env
import           Application.Options.Types
import qualified GitLab.EndPoint.Project   as EndPoint
import           GitLab.Types

process ∷ ActionProject → IO ()
process = \case
  ActionProjectCreate namepath -> do
    g <- Env.gitlab
    let query = QProjectCreate namepath Nothing emptyProjectEdit
    EndPoint.create_ g query

