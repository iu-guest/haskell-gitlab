{-# LANGUAGE UnicodeSyntax #-}
module Application.Options.Project (parser) where
import Application.Options.Types
import Data.Semigroup
import GitLab.Types
import Options.Applicative

import Data.Maybe
import Data.These

parser ∷ ParserInfo ActionProject
parser = info options (progDesc "project management")

options ∷ Parser ActionProject
options = optionCreate

optionCreate ∷ Parser ActionProject
optionCreate = ActionProjectCreate <$> do
  let nameM = metavar "NAME" <> help "Name of project"
      nameO = Name <$> (strArgument nameM)

  let pathM = long "path" <> help "Path of project"
      pathO = fmap (Just . Path) (strOption pathM) <|> pure Nothing

  let merge name = maybe (This name) (These name)
  merge <$> nameO <*> pathO
