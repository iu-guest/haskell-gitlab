{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE UnicodeSyntax #-}
module Application.Options.SSH (parser) where
import Application.Options.Types
import Data.Semigroup
import GitLab.Types
import Options.Applicative

parser ∷ ParserInfo ActionSSH
parser = info options (progDesc "ssh keys management")

options ∷ Parser ActionSSH
options = optionList <|> optionDelete <|> optionAdd

optionList ∷ Parser ActionSSH
optionList = flag' ActionSSHList (long "list")

optionDelete ∷ Parser ActionSSH
optionDelete = do
  let mods = metavar "ID" <> help "identifier of key to delete"
  flag' () (long "delete")
    *> (ActionSSHDelete <$> (argument auto mods))

optionAdd ∷ Parser ActionSSH
optionAdd = do
  let titleM = long "title" <> help "title of ssh key. Purely informative."
      titleO = strOption titleM

  let keyM = long "key" <> help "public ssh key (very long string)"
      keyO = strOption keyM

  let pathM = long "keypath" <> help "path to ssh key (like ~/.ssh/id_rsa.pub)"
      pathO = strOption pathM
  flag' () (long "add")
    *> (ActionSSHAdd <$> titleO <*> (fmap Left keyO <|> fmap Right pathO))
