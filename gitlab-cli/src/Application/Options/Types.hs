{-# LANGUAGE UnicodeSyntax #-}
module Application.Options.Types where
import Data.Text    (Text)
import Data.These
import GitLab.Types

data ActionSSH
  = ActionSSHList
  | ActionSSHDelete Int
  | ActionSSHAdd { title :: Text, key :: Either Text FilePath }

data ActionProject
  = ActionProjectCreate (These Name Path)

data Action
  = ActionProject ActionProject
  | ActionSSH ActionSSH
