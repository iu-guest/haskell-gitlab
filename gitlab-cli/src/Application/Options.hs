{-# LANGUAGE UnicodeSyntax #-}
module Application.Options (parser) where
import qualified Application.Options.Project as Project
import qualified Application.Options.SSH     as SSH
import           Application.Options.Types
import           Data.Semigroup
import           Options.Applicative

parser ∷ ParserInfo Action
parser = info (options <**> helper) meta where
  meta = fullDesc <> progDesc "Command line interface to GitLab instances"
  options = hsubparser $ mconcat [
      command "project" (ActionProject <$> Project.parser),
      command "ssh" (ActionSSH <$> SSH.parser)
    ]
